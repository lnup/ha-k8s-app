terraform {
  cloud {
    organization = "Olimp-TF"
    workspaces {
      name = "ha-k8s-app"
    }
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.11.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.14.0"
    }
  }
}
provider "aws" {
  region = var.region
  # access_key = var.aws_access_key
  # secret_key = var.aws_secret_key
  # profile = "176628222452_AdministratorAccess"
}